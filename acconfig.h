#undef HAVE_LIBSM
#undef PACKAGE
#undef VERSION
#undef ENABLE_NLS
#undef HAVE_CATGETS
#undef HAVE_GETTEXT
#undef HAVE_LC_MESSAGES
#undef HAVE_STPCPY

/* Define if the AudioFile driver is compiled */
#undef DRV_AF
/* Define if the AIX audio driver is compiled */
#undef DRV_AIX
/* Define if the Linux ALSA driver is compiled */
#undef DRV_ALSA
/* Define if the Enlightened Sound Daemon driver is compiled */
#undef DRV_ESD
/* Define if the HP-UX audio driver is compiled */
#undef DRV_HP
/* Define if the Open Sound System driver is compiled */
#undef DRV_OSS
/* Define if the SGI audio driver is compiled */
#undef DRV_SGI
/* Define if the Sun audio driver or compatible (NetBSD, OpenBSD)
   is compiled */
#undef DRV_SUN
/* Define if the Sun audio driver should be compiled with output on the
   headphone connector */
#undef USE_HEADPHONE
#undef SUNOS
