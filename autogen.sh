#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="trackerG"

(test -f $srcdir/configure.in \
  && test -f $srcdir/README \
  && test -f $srcdir/app/tg_main.c) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level trackerG directory"
    exit 1
}

. $srcdir/macros/autogen.sh
