#ifndef __TG_MENU_H__
#define __TG_MENU_H__

void tg_menu_new (GtkWidget **menubar, GtkAcceleratorTable **table, 
		  GtkMenuEntry *m_items, gint nm_items);
void tg_menu_create (GtkMenuFactory *, GtkMenuEntry *entries, int nmenu_entries);
    
#endif /* __TG_MENU_H__ */
