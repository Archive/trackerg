#ifndef __TG_QUIT_H__
#define __TG_QUIT_H__

void tg_quit_callback (GtkWidget *widget, gpointer data);
void tg_quit (void);

#endif __TG_QUIT_H__
