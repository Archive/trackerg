#include <stdarg.h>

#include <gnome.h>

#include "tg_intf.h"
#include "tg_quit.h"
#include "tg_open.h"
#include "tg_play.h"

/* The menu information for trackerG */
GnomeUIInfo file_menu[] = {
	{GNOME_APP_UI_ITEM, 
	N_("New..."), NULL, 
	NULL, NULL, NULL,
	GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_NEW,
	'N', GDK_CONTROL_MASK, NULL},
	{GNOME_APP_UI_ITEM,
	N_("Open..."), NULL,
	tg_open_callback, NULL, NULL,
	GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_OPEN,
	'O', GDK_CONTROL_MASK, NULL},
	GNOMEUIINFO_SEPARATOR,
	{GNOME_APP_UI_ITEM,
	N_("Quit"), NULL,
	tg_quit_callback, NULL, NULL,
	GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_QUIT,
	'Q', GDK_CONTROL_MASK, NULL},
	GNOMEUIINFO_END
};

GnomeUIInfo play_menu[] = {
	{GNOME_APP_UI_ITEM,
	N_("Play"), NULL,
	tg_playback_start, NULL, NULL,
	GNOME_APP_PIXMAP_NONE, NULL,
	'P', GDK_CONTROL_MASK, NULL},
	GNOMEUIINFO_END
};

GnomeUIInfo mainmenu[] = {
	GNOMEUIINFO_SUBTREE("File", file_menu),
	GNOMEUIINFO_SUBTREE("Playback", play_menu),
	GNOMEUIINFO_END
};

static gint tg_interface_delete (void)
{
    return(FALSE);
}


void tg_interface_init(void)
{
    GtkWidget *app;
    GtkWidget *hbox;
    GtkWidget *clist;
    
    gchar buf[1024];
    
    /* main window */
    app = gnome_app_new ("trackerG", "trackerG");

    gtk_signal_connect(GTK_OBJECT(app), "destroy",
		       GTK_SIGNAL_FUNC(tg_quit), NULL);
    gtk_signal_connect(GTK_OBJECT(app), "delete_event",
		       GTK_SIGNAL_FUNC(tg_interface_delete), NULL);
    
    /* menubar */
    gnome_app_create_menus(GNOME_APP(app), mainmenu);

    /* add a horiz box */
    hbox = gtk_hbox_new(2, FALSE);
    gnome_app_set_contents( GNOME_APP(app), hbox);

    /* Add the clist */
/*    clist = tg_create_patdisp(); */
    gtk_box_pack_start(GTK_BOX(hbox), clist, FALSE, TRUE, 0);

    /* show window */
/*    gtk_widget_show (clist); */
    gtk_widget_show (app);

}
