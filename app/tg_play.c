/*
	MIKMOD src shamlessly ripped up
	for GTK use.
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <gnome.h>

#include <signal.h>
#include <pthread.h>
#include "tg_quit.h"
#include "mikmod.h"

/* Something Owen wanted me to play with... */
#define MIN_INTERVAL 50
#define INCREASE_MULTIPLIER 1.5
#define DECREASE_MULTIPLIER 1.1

static int idle_called = TRUE;
static double interval = MIN_INTERVAL;

int quiet=0;           /* set if quiet mode is enabled */
UBYTE md_type=0;       /* default is a non-wavetable sound device */

int dorandom=0, delafterplay=0;
int semiquiet=0;
BOOL next;
MODULE *mf=NULL;


int idle_handler (gpointer data)
{
        idle_called = TRUE;
        return FALSE;
}

int tg_play(char *filename[])
{
  int quit;
  int cmderr=0;                   /* error in commandline flag */
  int morehelp=0;                 /* set if user wants more help */
  int t, n, foo;
  extern float speed_constant;   /* tempo multiplier, initialised to 1*/
  FILE *file;
  static BOOL  cfg_extspd  = 1,      /* Extended Speed enable */
    cfg_panning = 1,      /* DMP panning enable (8xx effects) */
    cfg_loop    = 0;      /* auto song-looping disable */
  int cfg_maxchn=64, c;

  /*
    Initialize soundcard parameters.. you _have_ to do this
    before calling MD_Init(), and it's illegal to change them
    after you've called MD_Init()
  */

  int tolerant    =0;
  md_mixfreq      = 44100;            /* standard mixing freq */
  md_device       = 0;                /* standard device: autodetect */
  md_volume       = 96;               /* driver volume (max 128) */
  md_musicvolume  = 128;              /* music volume (max 128) */
  md_sndfxvolume  = 128;              /* sound effects volume (max 128) */
  md_pansep       = 128;              /* panning separation (0 = mono, 128 = full stereo) */
  md_reverb       = 0;               /* Reverb (max 15) */
  md_mode = DMODE_16BITS | 
    DMODE_STEREO | 
    DMODE_SOFT_MUSIC;  /* default mixing mode */
  
  /* Register the loaders we want to use..  */
  MikMod_RegisterAllLoaders();
  
  /* Register the drivers we want to use: */
  MikMod_RegisterAllDrivers();
  
  MikMod_Init();
  
  if((mf=Player_Load(filename,cfg_maxchn,TRUE)) == NULL)
    {   
      mf->extspd  = cfg_extspd;
      mf->panflag = cfg_panning;
      mf->loop    = cfg_loop;
    }
  
  g_print("Flags: 0x%X\n", mf->flags);
  g_print("# of Channels: %d\n", mf->numchn);
  g_print("# of Voices: %d\n", mf->numvoices);
  g_print("# of Positions: %d\n", mf->numpos);
  g_print("# of Patterns: %d\n", mf->numpat);
  g_print("# of Tracks: %d\n", mf->numtrk);
  g_print("# of Inst: %d\n", mf->numins);
  g_print("# of Samples: %d\n", mf->numsmp);
  g_print("Name: %s\n", mf->songname);
  g_print("Comment: \n%s\n", mf->comment);
  
  for (foo = 0; foo < mf->numtrk; foo++) {
    g_print("Pointer %d: 0x%lX\n", foo, mf->tracks[foo]);
  }
  
  Player_Start(mf);
  return 0;
}

void tg_play2(void) {
  Player_Start(mf);
  
  next=0;
  
  /* if we have a quit signal, exit loop, and try to kludge
     around that damn "over the edge" bug... */
  while(Player_Active() && !next && mf->numpos<256)
    { 
      
      MikMod_Update();
      
      usleep(500);
      
      if (!md_type) /* handled elsewhere for GUS cards */
	{
				/*display_status();*/
	}
    }
  Player_Stop();          /* stop playing */
  Player_Free(mf);            /* and free the module */
  
  MikMod_Exit();
  MikMod_Update();
  return;
}

/* the grunt of this scheme */
int timeout_handler (gpointer data)
{
  MikMod_Update();
  
  if (!idle_called) {
    interval *= INCREASE_MULTIPLIER;
    gtk_timeout_add (interval, timeout_handler, NULL);
    return FALSE;
  } else {
    idle_called = FALSE;
    gtk_idle_add (idle_handler, NULL);
    if (interval > MIN_INTERVAL) {
      interval /= DECREASE_MULTIPLIER;
      gtk_timeout_add (interval, timeout_handler, NULL);
      return FALSE;
    } else 
      return TRUE;
  }
}

void tg_playback_start (void)
{
  Player_Start(mf);
  
  next=0;
  
  /* if we have a quit signal, exit loop, and try to kludge
     around that damn "over the edge" bug... */
  while(Player_Active() && !next && mf->numpos<256)
    {
      
      MikMod_Update();
      
      usleep(500);
      
      if (!md_type) /* handled elsewhere for GUS cards */
	{
	  if (gtk_events_pending())
	    gtk_main_iteration();
	}
    }
  Player_Stop();          /* stop playing */
  Player_Free(mf);            /* and free the module */
  
  MikMod_Exit();
  MikMod_Update();
  return;
  
#if 0
  gtk_timeout_add (interval, timeout_handler, NULL);
  /* #else */
  /* Try threads */
  pthread_t thread;
  struct sched_param *thread_param;
  
  thread_param->sched_priority = -20;
  
  pthread_create (&thread, NULL, (void *) tg_play2, NULL);
  pthread_setschedparam(thread, SCHED_FIFO, thread_param);
  
#endif
}

