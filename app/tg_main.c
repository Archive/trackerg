#include <config.h>
#include <gnome.h>

#include "tg_intf.h"

int main (int argc, char *argv[])
{
	/* Start up GTK and Gnome */
	gnome_init ("trackerg", "0.0.1", argc, argv);
	tg_interface_init();
	gtk_main();
}
	
