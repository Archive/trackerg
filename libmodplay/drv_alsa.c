/*	MikMod sound library
	(c) 1998 Miodrag Vallat and others - see file AUTHORS for complete list

	This library is free software; you can redistribute it and/or modify
	it under the terms of the GNU Library General Public License as
	published by the Free Software Foundation; either version 2 of
	the License, or (at your option) any later version.
 
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Library General Public License for more details.
 
	You should have received a copy of the GNU Library General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

/*==============================================================================

  $Id$

  MikMod driver for Advanced Linux Sound Architecture (ALSA)

==============================================================================*/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <stdlib.h>
#include <string.h>

#include <sys/asoundlib.h>

#include <mikmod_internals.h>

#define DEFAULT_NUMFRAGS 4

static	void* pcm_h=NULL;
static	int fragmentsize,numfrags=DEFAULT_NUMFRAGS;
static	SBYTE *audiobuffer=NULL;

static BOOL ALSA_IsThere(void)
{
	return (snd_cards_mask())?1:0;
}

static BOOL ALSA_Init(void)
{
	snd_pcm_format_t pformat;
	int mask,card;
	int cardmin=0,cardmax=SND_CARDS;
	int device=-1;

	/* adjust user-configurable settings */
	if(getenv("MM_NUMFRAGS")) {
		numfrags=atoi(getenv("MM_NUMFRAGS"));
		if ((numfrags<2)||(numfrags>16)) numfrags=DEFAULT_NUMFRAGS;
	}
	if(getenv("ALSA_CARD")) {
		cardmin=atoi(getenv("ALSA_CARD"));
		cardmax=cardmin+1;
		if(getenv("ALSA_PCM"))
			device=atoi(getenv("ALSA_PCM"));
	}

	/* setup playback format structure */
	memset(&pformat,0,sizeof(pformat));
	pformat.format=(md_mode&DMODE_16BITS)?SND_PCM_SFMT_S16_LE:SND_PCM_SFMT_U8;
	pformat.channels=(md_mode&DMODE_STEREO)?2:1;
	pformat.rate=md_mixfreq;

	/* scan for appropriate sound card */
	mask=snd_cards_mask();
	_mm_errno=MMERR_OPENING_AUDIO;
	for (card=cardmin;card<cardmax;card++) {
		struct snd_ctl_hw_info info;
		void *ctl_h;
		int dev,devmin,devmax;

		/* no card here, onto the next */
		if (!(mask&(1<<card))) continue;

		/* try to open the card in query mode */
		if(snd_ctl_open(&ctl_h,card)<0) {
#ifdef MIKMOD_DEBUG
			fprintf(stderr,"\rfailed to query card %d\n",card);
#endif
			continue;
		}

		/* get hardware information */
		if(snd_ctl_hw_info(ctl_h,&info)<0) {
#ifdef MIKMOD_DEBUG
			fprintf(stderr,"\rfailed to get hardware information for card %d\n",card);
#endif
			snd_ctl_close(ctl_h);
			continue;
		}

		/* scan subdevices */
		if(device==-1) {
			devmin=0;devmax=info.pcmdevs;
		} else
			devmin=devmax=device;
		for(dev=devmin;dev<devmax;dev++) {
			snd_pcm_info_t pcminfo;
			snd_pcm_playback_info_t ctlinfo;
			struct snd_pcm_playback_info pinfo;
			struct snd_pcm_playback_params pparams;
			int size,bps;

			/* get PCM capabilities */
			if(snd_ctl_pcm_info(ctl_h,dev,&pcminfo)<0) {
#ifdef MIKMOD_DEBUG
				fprintf(stderr,"\rfailed to get pcm caps for card %d, device %d\n",card,dev);
#endif
				continue;
			}

			/* look for playback capability */
			if(!(pcminfo.flags&SND_PCM_INFO_PLAYBACK)) {
#ifdef MIKMOD_DEBUG
				fprintf(stderr,"\rno playback capability for card %d, device %d\n",card,dev);
#endif
				continue;
			}

			/* get playback information */
			if(snd_ctl_pcm_playback_info(ctl_h,dev,&ctlinfo)<0) {
#ifdef MIKMOD_DEBUG
				fprintf(stderr,"\rfailed to get playback information for card %d, device %d\n",card,dev);
#endif
				continue;
			}

	/*
	   If control goes here, we have found a sound device able to play PCM data.
	   Let's open in in playback mode and see if we have compatible playback
	   settings.
	*/

			if (snd_pcm_open(&pcm_h,card,dev,SND_PCM_OPEN_PLAYBACK)<0) {
#ifdef MIKMOD_DEBUG
				fprintf(stderr,"\rfailed to open card %d, device %d\n",card,dev);
#endif
				continue;
			}

			if (snd_pcm_playback_info(pcm_h,&pinfo)<0) {
#ifdef MIKMOD_DEBUG
				fprintf(stderr,"\rfailed to get pcm playback information for card %d, device %d\n",card,dev);
#endif
				snd_pcm_close(pcm_h);
				pcm_h=NULL;
				continue;
			}

			/* check we have compatible settings */
			if((pinfo.min_rate>pformat.rate)||(pinfo.max_rate<pformat.rate)||
			   (!(pinfo.formats&(1<<pformat.format)))) {
#ifdef MIKMOD_DEBUG
				fprintf(stderr,"\rincompatible settings for card %d, device %d\n",card,dev);
#endif
				snd_pcm_close(pcm_h);
				pcm_h=NULL;
				continue;
			}

			fragmentsize=pinfo.buffer_size/numfrags;
#ifdef MIKMOD_DEBUG
			if ((fragmentsize<512)||(fragmentsize>16777216L))
				fprintf(stderr,"\rweird pinfo.buffer_size:%d\n",pinfo.buffer_size);
#endif

			snd_pcm_flush_playback(pcm_h);

			/* set new parameters */
			if(snd_pcm_playback_format(pcm_h,&pformat)<0) {
#ifdef MIKMOD_DEBUG
				fprintf(stderr,"\rfailed to set pcm format for card %d, device %d\n",card,dev);
#endif
				snd_pcm_close(pcm_h);
				pcm_h=NULL;
				continue;
			}

			/* compute a fragmentsize hint
			   each fragment should be shorter than, but close to, half a
			   second of playback */
			bps=(pformat.rate*pformat.channels*(md_mode&DMODE_16BITS?2:1))>>1;
			size=fragmentsize;while (size>bps) size>>=1;
#ifdef MIKMOD_DEBUG
			if (size<16) {
				fprintf(stderr,"\rweird hint result:%d from %d, bps=%d\n",size,fragmentsize,bps);
				size=16;
			}
#endif

			memset(&pparams,0,sizeof(pparams));
			pparams.fragment_size=size;
			pparams.fragments_max=-1; /* choose the best */
			pparams.fragments_room=-1;
			if(snd_pcm_playback_params(pcm_h,&pparams)<0) {
#ifdef MIKMOD_DEBUG
				fprintf(stderr,"\rfailed to set playback parameters for card %d, device %d\n",card,dev);
#endif
				snd_pcm_close(pcm_h);
				pcm_h=NULL;
				continue;
			}

			if (!(audiobuffer=(SBYTE*)_mm_malloc(fragmentsize))) {
				snd_ctl_close(ctl_h);
				return 1;
			}

			/* sound device is ready to work */
			if (VC_Init()) {
				snd_ctl_close(ctl_h);
				return 1;
			} else
			  return 0;
		}

		snd_ctl_close(ctl_h);
	}
	return 1;
}

static void ALSA_Exit(void)
{
	VC_Exit();
	if (pcm_h) {
		snd_pcm_close(pcm_h);
		pcm_h=NULL;
	}
	if (audiobuffer) {
		free(audiobuffer);
		audiobuffer=NULL;
	}
}

static void ALSA_Update(void)
{
	snd_pcm_playback_status_t status;

	if(snd_pcm_playback_status(pcm_h,&status)>=0) {
		if(status.count>fragmentsize) status.count=fragmentsize;
		snd_pcm_write(pcm_h,audiobuffer,
		      VC_WriteBytes(audiobuffer,status.count));
	}
}

static BOOL ALSA_Reset(void)
{
	ALSA_Exit();
	return ALSA_Init();
}

MDRIVER drv_alsa={
	NULL,
	"ALSA",
	"Advanced Linux Sound Architecture (ALSA) driver v0.2.0-pre8",
	0,255,
	ALSA_IsThere,
	VC_SampleLoad,
	VC_SampleUnload,
	VC_SampleSpace,
	VC_SampleLength,
	ALSA_Init,
	ALSA_Exit,
	ALSA_Reset,
	VC_SetNumVoices,
	VC_PlayStart,
	VC_PlayStop,
	ALSA_Update,
	VC_VoiceSetVolume,
	VC_VoiceSetFrequency,
	VC_VoiceSetPanning,
	VC_VoicePlay,
	VC_VoiceStop,
	VC_VoiceStopped,
	VC_VoiceGetPosition,
	VC_VoiceRealVolume
};
