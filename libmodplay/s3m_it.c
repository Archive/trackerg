/*	MikMod sound library
	(c) 1998 Miodrag Vallat and others - see file AUTHORS for complete list

	This library is free software; you can redistribute it and/or modify
	it under the terms of the GNU Library General Public License as
	published by the Free Software Foundation; either version 2 of
	the License, or (at your option) any later version.
 
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Library General Public License for more details.
 
	You should have received a copy of the GNU Library General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

/*==============================================================================

  $Id$

  Non-protracker player parts (common functions between S3Ms and ITs)

==============================================================================*/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <mikmod_internals.h>

SBYTE  remap[64];           /* for removing empty channels */
UBYTE* poslookup=NULL;      /* lookup table for pattern jumps after blank
                               pattern removal */
UBYTE  poslookupcnt;

BOOL   filters;             /* resonant filters in use */
UBYTE  activemacro;         /* active midi macro number for Sxx,xx<80h */
UBYTE  filtermacros[16];    /* midi macros settings */
FILTER filtersettings[256]; /* computed filter settings */

void S3MIT_ProcessCmd(UBYTE cmd,UBYTE inf,BOOL oldeffect)
{
	UBYTE hi,lo;

	lo=inf&0xf;
	hi=inf>>4;

	/* process S3M / IT specific command structure */

	if(cmd!=255) {
		switch(cmd) {
			case 1: /* Axx set speed to xx */
				UniWrite(UNI_S3MEFFECTA);
				UniWrite(inf);
				break;
			case 2: /* Bxx position jump */
				if (inf<poslookupcnt)
					UniPTEffect(0xb,poslookup[inf]);
				break;
			case 3: /* Cxx patternbreak to row xx */
				if(oldeffect & 1)
					UniPTEffect(0xd,(((inf&0xf0)>>4)*10)+(inf&0xf));
				else
					UniPTEffect(0xd,inf);
				break;
			case 4: /* Dxy volumeslide */
				UniWrite(UNI_S3MEFFECTD);
				UniWrite(inf);
				break;
			case 5: /* Exy toneslide down */
				UniWrite(UNI_S3MEFFECTE);
				UniWrite(inf);
				break;
			case 6: /* Fxy toneslide up */
				UniWrite(UNI_S3MEFFECTF);
				UniWrite(inf);
				break;
			case 7: /* Gxx Tone portamento, speed xx */
				UniWrite(UNI_ITEFFECTG);
				UniWrite(inf);
				break;
			case 8: /* Hxy vibrato */
				if(oldeffect & 1)
					UniPTEffect(0x4,inf);
				else {
					UniWrite(UNI_ITEFFECTH);
					UniWrite(inf);
				}
				break;
			case 9: /* Ixy tremor, ontime x, offtime y */
				if(oldeffect & 1)
					UniWrite(UNI_S3MEFFECTI);
				else                     
					UniWrite(UNI_ITEFFECTI);
				UniWrite(inf);
				break;
			case 0xa: /* Jxy arpeggio */
				UniPTEffect(0x0,inf);
				break;
			case 0xb: /* Kxy Dual command H00 & Dxy */
				if(oldeffect & 1)
					UniPTEffect(0x4,0);    
				else {
					UniWrite(UNI_ITEFFECTH);
					UniWrite(0);
				}
				UniWrite(UNI_S3MEFFECTD);
				UniWrite(inf);
				break;
			case 0xc: /* Lxy Dual command G00 & Dxy */
				if(oldeffect & 1)
					UniPTEffect(0x3,0);
				else {
					UniWrite(UNI_ITEFFECTG);
					UniWrite(0);
				}
				UniWrite(UNI_S3MEFFECTD);
				UniWrite(inf);
				break;
			case 0xd: /* Mxx Set Channel Volume */
				UniWrite(UNI_ITEFFECTM);
				UniWrite(inf);
				break;       
			case 0xe: /* Nxy Slide Channel Volume */
				UniWrite(UNI_ITEFFECTN);
				UniWrite(inf);
				break;
			case 0xf: /* Oxx set sampleoffset xx00h */
				UniPTEffect(0x9,inf);
				break;
			case 0x10: /* Pxy Slide Panning Commands */
				UniWrite(UNI_ITEFFECTP);
				UniWrite(inf);
				break;
			case 0x11: /* Qxy Retrig (+volumeslide) */
				UniWrite(UNI_S3MEFFECTQ);
				if((inf) && (!lo) && !(oldeffect & 1))
					UniWrite(1);
				else
					UniWrite(inf); 
				break;
			case 0x12: /* Rxy tremolo speed x, depth y */
				UniWrite(UNI_S3MEFFECTR);
				UniWrite(inf);
				break;
			case 0x13: /* Sxx special commands */
				if (inf>=0xf0) {
					/* change resonant filter settings if necessary */
					if((filters)&&((inf&0xf)!=activemacro)) {
						activemacro=inf&0xf;
						for(inf=0;inf<0x80;inf++)
							filtersettings[inf].filter=filtermacros[activemacro];
					}
				} else {
					UniWrite(UNI_ITEFFECTS0);
					UniWrite(inf);
				}
				break;
			case 0x14: /* Txx tempo */
				if(inf>0x20) {
					UniWrite(UNI_S3MEFFECTT);
					UniWrite(inf);
				}
				break;
			case 0x15: /* Uxy Fine Vibrato speed x, depth y */
				if(oldeffect & 1)
					UniWrite(UNI_S3MEFFECTU);
				else
					UniWrite(UNI_ITEFFECTU);
				UniWrite(inf);
				break;
			case 0x16: /* Vxx Set Global Volume */
				UniWrite(UNI_XMEFFECTG);
				UniWrite(inf);
				break;
			case 0x17: /* Wxy Global Volume Slide */
				UniWrite(UNI_ITEFFECTW);
				UniWrite(inf);  
				break;
			case 0x18: /* Xxx amiga command 8xx */
				if(oldeffect & 1)
					UniPTEffect(0x8,(inf==128)?255:(inf<<1));
				else
					UniPTEffect(0x8,inf);
				break;
			case 0x19: /* Yxy Panbrello  speed x, depth y */
				UniWrite(UNI_ITEFFECTY);
				UniWrite(inf);
				break;
			case 0x1a: /* Zxx midi/resonant filters */
				if(filtersettings[inf].filter) {
					UniWrite(UNI_ITEFFECTZ);
					UniWrite(filtersettings[inf].filter);
					UniWrite(filtersettings[inf].inf);
				}
				break;
		}
	}
}
