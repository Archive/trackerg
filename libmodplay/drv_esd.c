/*	MikMod sound library
	(c) 1998 Miodrag Vallat and others - see file AUTHORS for complete list

	This library is free software; you can redistribute it and/or modify
	it under the terms of the GNU Library General Public License as
	published by the Free Software Foundation; either version 2 of
	the License, or (at your option) any later version.
 
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Library General Public License for more details.
 
	You should have received a copy of the GNU Library General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

/*==============================================================================

  $Id$

  Mikmod driver for the Enlightened sound daemon (EsounD)

==============================================================================*/

/*

	You should set the hostname of the machine running esd in the environment
	variable 'ESPEAKER'. If this variable is not set, localhost is used.

*/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <errno.h>
#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif
#include <stdlib.h>
#include <signal.h>
#include <time.h>

#include <esd.h>

#include <mikmod_internals.h>

static	int sndfd=-1;
static	esd_format_t format;
static	SBYTE *audiobuffer=NULL;

#ifndef HAVE_ESD_CLOSE
#define esd_close(x) close(x)
#endif

#ifndef ESD_DEFAULT_RATE
#define ESD_DEFAULT_RATE (44100)
#endif

/* I hope to have this function integrated into libesd someday...*/
static ssize_t esd_writebuf(int fd,const void *buffer,size_t count)
{
	ssize_t start=0;

	while(start<count) {
		ssize_t res;

		res=write(fd,(char*)buffer+start,count-start);
		if(res<0) {
			/* connection lost */
			if(errno==EPIPE)
				return -1-start;
		} else
			start+=res;
	}
	return start;
}

static BOOL ESD_IsThere(void)
{
	int fd;

	/* Try to esablish a connection with default esd settings */
	if((fd=esd_play_stream(ESD_BITS16|ESD_STEREO|ESD_STREAM|ESD_PLAY,
	                       ESD_DEFAULT_RATE,NULL,"MikMod"))<0)
		return 0;

	esd_close(fd);
	return 1;
}

static BOOL ESD_Init(void)
{
	format=(md_mode&DMODE_16BITS?ESD_BITS16:ESD_BITS8)|
	       (md_mode&DMODE_STEREO?ESD_STEREO:ESD_MONO)|ESD_STREAM|ESD_PLAY;

	/* make sure we can open an esd stream with our parameters */
	if((sndfd=esd_play_stream(format,md_mixfreq,NULL,"MikMod"))<0) {
		_mm_errno=MMERR_OPENING_AUDIO;
		return 1;
	}

	if(!(audiobuffer=(SBYTE*)_mm_malloc(ESD_BUF_SIZE*sizeof(char))))
		return 1;

	return VC_Init();
}

static void ESD_Exit(void)
{
	VC_Exit();
	if (audiobuffer) {
		free(audiobuffer);
		audiobuffer=NULL;
	}
	if (sndfd>=0) {
		esd_close(sndfd);
		sndfd=-1;
	}
}

static void ESD_Update(void)
{
	int count=VC_WriteBytes(audiobuffer,ESD_BUF_SIZE);
	static time_t losttime;

	if(sndfd>=0) {
		if(esd_writebuf(sndfd,audiobuffer,count)<0) {
			/* if we lost our connection with esd, clean up and work as the
			   nosound driver until we can reconnect */
			esd_close(sndfd);
			sndfd=-1;
			losttime=time(NULL);
		}
	} else {
		/* an alarm would be better, but then the library user could not use
		   alarm(2) himself... */
		if (time(NULL)-losttime>=5) {
			losttime=time(NULL);
			/* Attempt to reconnect every 5 seconds */
			if((sndfd=esd_play_stream(format,md_mixfreq,NULL,"MikMod"))>=0) {
				VC_SilenceBytes(audiobuffer,ESD_BUF_SIZE);
				esd_writebuf(sndfd,audiobuffer,ESD_BUF_SIZE);
			}
		}
	}
}

static BOOL ESD_PlayStart(void)
{
	if(sndfd<0)
		if((sndfd=esd_play_stream(format,md_mixfreq,NULL,"MikMod"))<0) {
			_mm_errno=MMERR_OPENING_AUDIO;
			return 1;
		}
	/* since the default behaviour of SIGPIPE on most Unices is to kill the
	   program, we'll prefer handle EPIPE ourselves should the esd die */
	signal(SIGPIPE,SIG_IGN);

	/* silence buffers */
	VC_SilenceBytes(audiobuffer,ESD_BUF_SIZE);
	esd_writebuf(sndfd,audiobuffer,ESD_BUF_SIZE);

	return VC_PlayStart();
}

static void ESD_PlayStop(void)
{
	if (sndfd>=0) {
		/* silence buffers */
		VC_SilenceBytes(audiobuffer,ESD_BUF_SIZE);
		esd_writebuf(sndfd,audiobuffer,ESD_BUF_SIZE);

		signal(SIGPIPE,SIG_DFL);
	}

	VC_PlayStop();
}

static BOOL ESD_Reset(void)
{
	ESD_Exit();
	return ESD_Init();
}

MDRIVER drv_esd={
	NULL,
	"Enlightened sound daemon",
	/* use the same version number as the EsounD release it works with */
	"Enlightened sound daemon (EsounD) driver v0.2.6",
	0,255,
	ESD_IsThere,
	VC_SampleLoad,
	VC_SampleUnload,
	VC_SampleSpace,
	VC_SampleLength,
	ESD_Init,
	ESD_Exit,
	ESD_Reset,
	VC_SetNumVoices,
	ESD_PlayStart,
	ESD_PlayStop,
	ESD_Update,
	VC_VoiceSetVolume,
	VC_VoiceSetFrequency,
	VC_VoiceSetPanning,
	VC_VoicePlay,
	VC_VoiceStop,
	VC_VoiceStopped,
	VC_VoiceGetPosition,
	VC_VoiceRealVolume
};
