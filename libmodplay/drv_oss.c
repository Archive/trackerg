/*	MikMod sound library
	(c) 1998 Miodrag Vallat and others - see file AUTHORS for complete list

	This library is free software; you can redistribute it and/or modify
	it under the terms of the GNU Library General Public License as
	published by the Free Software Foundation; either version 2 of
	the License, or (at your option) any later version.
 
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Library General Public License for more details.
 
	You should have received a copy of the GNU Library General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

/*==============================================================================

  $Id$

  Mikmod driver for output on linux and FreeBSD Open Sound System (OSS)
  (/dev/dsp) 

==============================================================================*/

/*

	Written by Chris Conn <cconn@tohs.abacom.com>

	You can use the environment variables 'MM_FRAGSIZE' and 'MM_NUMFRAGS' to 
	override the default size & number of audio buffer fragments. If you 
	experience crackles & pops, try experimenting with these values.

	In general, the slower your system, the higher these values need to be. 

	MM_NUMFRAGS is within the range 2 to 255 (decimal)
	MM_FRAGSIZE is is within the range 7 to 17 (dec).
	The requested fragment size will be 2^MM_FRAGSIZE

*/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <errno.h>
#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif
#include <stdlib.h>
#ifdef HAVE_SYS_IOCTL_H
#include <sys/ioctl.h>
#endif
#ifdef __FreeBSD__
#include <machine/soundcard.h>
#else 
#include <sys/soundcard.h>
#endif

#include <mikmod_internals.h>

#define DEFAULT_FRAGSIZE 17
#define DEFAULT_NUMFRAGS 4

static	int sndfd=-1;
static	int fragmentsize;
static	SBYTE *audiobuffer=NULL;

static BOOL OSS_IsThere(void)
{
	/* under Linux, and perhaps other Unixes, access()ing the device is not
	   enough since it won't fail if the machine doesn't have sound support
	   in the kernel or sound hardware                                      */
	int fd;

	if((fd=open("/dev/dsp",O_WRONLY))>0) {
		close(fd);
		return 1;
	}
	return (errno==EACCES?1:0);
}

static BOOL OSS_Init(void)
{
	char *env;
	int play_precision,play_stereo,play_rate;
	int fragsize,numfrags;
	
	if((sndfd=open("/dev/dsp",O_WRONLY))<0) {
		_mm_errno=MMERR_OPENING_AUDIO;
		return 1;
	}

	fragsize=(env=getenv("MM_FRAGSIZE"))?atoi(env):DEFAULT_FRAGSIZE;
	numfrags=(env=getenv("MM_NUMFRAGS"))?atoi(env):DEFAULT_NUMFRAGS;
		
	if(fragsize<7||fragsize>17) fragsize=DEFAULT_FRAGSIZE;
	if(numfrags<2||numfrags>255) numfrags=DEFAULT_NUMFRAGS;

	fragmentsize=(numfrags<<16)|fragsize;
	
	if(ioctl(sndfd,SNDCTL_DSP_SETFRAGMENT,&fragmentsize)<0) {
		_mm_errno=MMERR_OSS_SETFRAGMENT;
		return 1;
	}

	play_precision=(md_mode&DMODE_16BITS)?16:8;
	play_stereo=(md_mode&DMODE_STEREO)?1:0;
	play_rate=md_mixfreq;

	if(ioctl(sndfd,SNDCTL_DSP_SAMPLESIZE,&play_precision)<0) {
		_mm_errno=MMERR_OSS_SETSAMPLESIZE;
		return 1;
	}
	if(ioctl(sndfd,SNDCTL_DSP_STEREO,&play_stereo)<0) {
		_mm_errno=MMERR_OSS_SETSTEREO;
		return 1;
	}
	if(ioctl(sndfd,SNDCTL_DSP_SPEED,&play_rate)<0) {
		_mm_errno=MMERR_OSS_SETSPEED;
		return 1;
	}

	ioctl(sndfd, SNDCTL_DSP_GETBLKSIZE, &fragmentsize);

	if(!(audiobuffer=(SBYTE*)_mm_malloc(fragmentsize*numfrags*sizeof(SBYTE)))) {
		return 1;
	}

	if(VC_Init()) {
		return 1;
	}
	return 0;
}

static void OSS_Exit(void)
{
	VC_Exit();
	if (audiobuffer) {
		free(audiobuffer);
		audiobuffer=NULL;
	}
	if (sndfd>=0) {
		close(sndfd);
		sndfd=-1;
	}
}

static void OSS_Update(void)
{
	audio_buf_info buffinf;

	ioctl(sndfd, SNDCTL_DSP_GETOSPACE,&buffinf);
	write(sndfd,audiobuffer,
	      VC_WriteBytes(audiobuffer,buffinf.fragments*buffinf.fragsize));
}

static BOOL OSS_Reset(void)
{
	VC_Exit();
	ioctl(sndfd,SNDCTL_DSP_RESET);
	return VC_Init();
}

MDRIVER drv_oss={
	NULL,
	"Open Sound System (OSS)",
	"Open Sound System (OSS) driver v1.3",
	0,255,
	OSS_IsThere,
	VC_SampleLoad,
	VC_SampleUnload,
	VC_SampleSpace,
	VC_SampleLength,
	OSS_Init,
	OSS_Exit,
	OSS_Reset,
	VC_SetNumVoices,
	VC_PlayStart,
	VC_PlayStop,
	OSS_Update,
	VC_VoiceSetVolume,
	VC_VoiceSetFrequency,
	VC_VoiceSetPanning,
	VC_VoicePlay,
	VC_VoiceStop,
	VC_VoiceStopped,
	VC_VoiceGetPosition,
	VC_VoiceRealVolume
};
