/*	MikMod sound library
	(c) 1998 Miodrag Vallat and others - see file AUTHORS for complete list

	This library is free software; you can redistribute it and/or modify
	it under the terms of the GNU Library General Public License as
	published by the Free Software Foundation; either version 2 of
	the License, or (at your option) any later version.
 
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Library General Public License for more details.
 
	You should have received a copy of the GNU Library General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

/*==============================================================================

  $Id$

  Portable file I/O routines

==============================================================================*/

/*

	The way this module works :

	- _mm_fopen will call the errorhandler [see mmerror.c] in addition to
	  setting _mm_errno on exit.
	- _mm_iobase is for internal use.  It is used by Player_LoadFP to
	  ensure that it works properly with wad files.
	- _mm_read_I_* and _mm_read_M_* differ : the first is for reading data
	  written by a little endian (intel) machine, and the second is for reading
	  big endian (Mac, RISC, Alpha) machine data.
	- _mm_write functions work the same as the _mm_read functions.
	- _mm_read_string is for reading binary strings.  It is basically the same
	  as an fread of bytes.

*/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <string.h>

#include <mikmod_internals.h>

#define COPY_BUFSIZE  1024

static long _mm_iobase=0,temp_iobase=0;

FILE* _mm_fopen(CHAR* fname,CHAR* attrib)
{
	FILE *fp;

	if(!(fp=fopen(fname,attrib))) {
		_mm_errno = MMERR_OPENING_FILE;
		if(_mm_errorhandler) _mm_errorhandler();
	}
	return fp;
}

int _mm_fseek(FILE* stream,long offset,int whence)
{
	return fseek(stream,(whence==SEEK_SET)?offset+_mm_iobase:offset,whence);
}

long _mm_ftell(FILE* stream)
{
	return ftell(stream)-_mm_iobase;
}

BOOL _mm_FileExists(CHAR* fname)
{
	FILE *fp;

	if(!(fp=fopen(fname,"r"))) return 0;
	fclose(fp);

	return 1;
}

/* Sets the current file-position as the new _mm_iobase */
void _mm_iobase_setcur(FILE* fp)
{
	temp_iobase=_mm_iobase;  /* store old value in case of revert */
	_mm_iobase=ftell(fp);
}

/* Reverts to the last known _mm_iobase value. */
void _mm_iobase_revert(void)
{
	_mm_iobase=temp_iobase;
}


/*========== Write functions */

void _mm_write_string(CHAR* data,FILE* fp)
{
	if(data)
		_mm_write_UBYTES(data,strlen(data),fp);
}

void _mm_write_M_UWORD(UWORD data,FILE* fp)
{
	_mm_write_UBYTE(data>>8,fp);
	_mm_write_UBYTE(data&0xff,fp);
}

void _mm_write_I_UWORD(UWORD data,FILE* fp)
{
	_mm_write_UBYTE(data&0xff,fp);
	_mm_write_UBYTE(data>>8,fp);
}

void _mm_write_M_ULONG(ULONG data,FILE* fp)
{
	_mm_write_M_UWORD(data>>16,fp);
	_mm_write_M_UWORD(data&0xffff,fp);
}

void _mm_write_I_ULONG(ULONG data,FILE* fp)
{
	_mm_write_I_UWORD(data&0xffff,fp);
	_mm_write_I_UWORD(data>>16,fp);
}

void _mm_write_M_SWORD(SWORD data,FILE* fp)
{
	_mm_write_M_UWORD((UWORD)data,fp);
}

void _mm_write_I_SWORD(SWORD data,FILE* fp)
{
	_mm_write_I_UWORD((UWORD)data,fp);
}

void _mm_write_M_SLONG(SLONG data,FILE* fp)
{
	_mm_write_M_ULONG((ULONG)data,fp);
}

void _mm_write_I_SLONG(SLONG data,FILE* fp)
{
	_mm_write_I_ULONG((ULONG)data,fp);
}

#define DEFINE_MULTIPLE_WRITE_FUNCTION(type_name,type)           \
void _mm_write_##type_name##S (type *buffer,int number,FILE* fp) \
{                                                                \
	while(number-->0)                                            \
		_mm_write_##type_name(*(buffer++),fp);                   \
}

/*DEFINE_MULTIPLE_WRITE_FUNCTION(SBYTE,SBYTE)*/
/*DEFINE_MULTIPLE_WRITE_FUNCTION(UBYTE,UBYTE)*/

DEFINE_MULTIPLE_WRITE_FUNCTION(M_SWORD,SWORD)
DEFINE_MULTIPLE_WRITE_FUNCTION(M_UWORD,UWORD)
DEFINE_MULTIPLE_WRITE_FUNCTION(I_SWORD,SWORD)
DEFINE_MULTIPLE_WRITE_FUNCTION(I_UWORD,UWORD)

DEFINE_MULTIPLE_WRITE_FUNCTION(M_SLONG,SLONG)
DEFINE_MULTIPLE_WRITE_FUNCTION(M_ULONG,ULONG)
DEFINE_MULTIPLE_WRITE_FUNCTION(I_SLONG,SLONG)
DEFINE_MULTIPLE_WRITE_FUNCTION(I_ULONG,ULONG)

/*========== Read functions */

int _mm_read_string(CHAR* buffer,int number,FILE* fp)
{
	fread(buffer,1,number,fp);
	return !feof(fp);
}

UWORD _mm_read_M_UWORD(FILE *fp)
{
	UWORD result=((UWORD)_mm_read_UBYTE(fp))<<8;
	result|=_mm_read_UBYTE(fp);
	return result;
}

UWORD _mm_read_I_UWORD(FILE *fp)
{
	UWORD result=_mm_read_UBYTE(fp);
	result|=((UWORD)_mm_read_UBYTE(fp))<<8;
	return result;
}

ULONG _mm_read_M_ULONG(FILE *fp)
{
	ULONG result=((ULONG)_mm_read_M_UWORD(fp))<<16;
	result|=_mm_read_M_UWORD(fp);
	return result;
}

ULONG _mm_read_I_ULONG(FILE *fp)
{
	ULONG result=_mm_read_I_UWORD(fp);
	result|=((ULONG)_mm_read_I_UWORD(fp))<<16;
	return result;
}

SWORD _mm_read_M_SWORD(FILE* fp)
{
	return((SWORD)_mm_read_M_UWORD(fp));
}

SWORD _mm_read_I_SWORD(FILE* fp)
{
	return((SWORD)_mm_read_I_UWORD(fp));
}

SLONG _mm_read_M_SLONG(FILE* fp)
{
	return((SLONG)_mm_read_M_ULONG(fp));
}

SLONG _mm_read_I_SLONG(FILE* fp)
{
	return((SLONG)_mm_read_I_ULONG(fp));
}

#define DEFINE_MULTIPLE_READ_FUNCTION(type_name,type)          \
int _mm_read_##type_name##S (type *buffer,int number,FILE* fp) \
{                                                              \
	while(number-->0)                                          \
		*(buffer++)=_mm_read_##type_name(fp);                  \
	return !feof(fp);                                          \
}

/*DEFINE_MULTIPLE_READ_FUNCTION(SBYTE,SBYTE)*/
/*DEFINE_MULTIPLE_READ_FUNCTION(UBYTE,UBYTE)*/

DEFINE_MULTIPLE_READ_FUNCTION(M_SWORD,SWORD)
DEFINE_MULTIPLE_READ_FUNCTION(M_UWORD,UWORD)
DEFINE_MULTIPLE_READ_FUNCTION(I_SWORD,SWORD)
DEFINE_MULTIPLE_READ_FUNCTION(I_UWORD,UWORD)

DEFINE_MULTIPLE_READ_FUNCTION(M_SLONG,SLONG)
DEFINE_MULTIPLE_READ_FUNCTION(M_ULONG,ULONG)
DEFINE_MULTIPLE_READ_FUNCTION(I_SLONG,SLONG)
DEFINE_MULTIPLE_READ_FUNCTION(I_ULONG,ULONG)

