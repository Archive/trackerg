/*	MikMod sound library
	(c) 1998 Miodrag Vallat and others - see file AUTHORS for complete list

	This library is free software; you can redistribute it and/or modify
	it under the terms of the GNU Library General Public License as
	published by the Free Software Foundation; either version 2 of
	the License, or (at your option) any later version.
 
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Library General Public License for more details.
 
	You should have received a copy of the GNU Library General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

/*==============================================================================

  $Id$

  Mikmod driver for output to a file called MUSIC.RAW

==============================================================================*/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif
#include <sys/types.h>
#include <sys/stat.h>

#include <mikmod_internals.h>

#define BUFFERSIZE 32768
#define FILENAME "music.raw"

static	int rawout=-1;
static	SBYTE *audiobuffer=NULL;

static BOOL RAW_IsThere(void)
{
	return 1;
}

static BOOL RAW_Init(void)
{
	if((rawout=open(FILENAME,O_RDWR|O_TRUNC|O_CREAT,S_IREAD|S_IWRITE))<0) {
		_mm_errno=MMERR_OPENING_FILE;
		return 1;
	}
	md_mode|=DMODE_SOFT_MUSIC|DMODE_SOFT_SNDFX;

	if (!(audiobuffer=(SBYTE*)_mm_malloc(BUFFERSIZE))) {
		close(rawout);unlink(FILENAME);
		rawout=-1;
		return 1;
	}

	if (VC_Init()) {
		close(rawout);unlink(FILENAME);
		rawout=-1;
		return 1;
	}
	return 0;
}

static void RAW_Exit(void)
{
	VC_Exit();
	if (rawout!=-1) {
		close(rawout);
		rawout=-1;
	}
	if (audiobuffer) {
		free(audiobuffer);
		audiobuffer=NULL;
	}
}

static void RAW_Update(void)
{
	write(rawout,audiobuffer,VC_WriteBytes(audiobuffer,BUFFERSIZE));
}

static BOOL RAW_Reset(void)
{
	close(rawout);
	if((rawout=open(FILENAME,O_RDWR|O_TRUNC|O_CREAT,S_IREAD|S_IWRITE))<0) {
		_mm_errno=MMERR_OPENING_FILE;
		return 1;
	}

	return 0;
}

MDRIVER drv_raw={
	NULL,
	"Disk writer (raw data)",
	"Raw disk writer (music.raw) v1.1",
	0,255,
	RAW_IsThere,
	VC_SampleLoad,
	VC_SampleUnload,
	VC_SampleSpace,
	VC_SampleLength,
	RAW_Init,
	RAW_Exit,
	RAW_Reset,
	VC_SetNumVoices,
	VC_PlayStart,
	VC_PlayStop,
	RAW_Update,
	VC_VoiceSetVolume,
	VC_VoiceSetFrequency,
	VC_VoiceSetPanning,
	VC_VoicePlay,
	VC_VoiceStop,
	VC_VoiceStopped,
	VC_VoiceGetPosition,
	VC_VoiceRealVolume
};

