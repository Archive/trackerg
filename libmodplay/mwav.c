/*	MikMod sound library
	(c) 1998 Miodrag Vallat and others - see file AUTHORS for complete list

	This library is free software; you can redistribute it and/or modify
	it under the terms of the GNU Library General Public License as
	published by the Free Software Foundation; either version 2 of
	the License, or (at your option) any later version.
 
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Library General Public License for more details.
 
	You should have received a copy of the GNU Library General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

/*==============================================================================

  $Id$

 WAV sample loader

==============================================================================*/

/*
   FIXME: Stereo .WAV files are not yet supported as samples.
*/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string.h>

#include <mikmod_internals.h>

typedef struct WAV {
	CHAR  rID[4];
	ULONG rLen;
	CHAR  wID[4];
	CHAR  fID[4];
	ULONG fLen;
	UWORD wFormatTag;
	UWORD nChannels;
	ULONG nSamplesPerSec;
	ULONG nAvgBytesPerSec;
	UWORD nBlockAlign;
	UWORD nFormatSpecific;
} WAV;

SAMPLE* Sample_LoadFP(FILE* fp)
{
	SAMPLE *si;
	WAV wh;
	CHAR dID[4];

	/* read wav header */
	_mm_read_string(wh.rID,4,fp);
	wh.rLen = _mm_read_I_ULONG(fp);
	_mm_read_string(wh.wID,4,fp);

	while(1) {
		_mm_read_string(wh.fID,4,fp);
		wh.fLen = _mm_read_I_ULONG(fp);
		if(!(memcmp(wh.fID,"fmt ",4))) break;
		_mm_fseek(fp,wh.fLen,SEEK_CUR);
	}

	if(feof(fp)|| memcmp(wh.rID,"RIFF",4) || memcmp(wh.wID,"WAVE",4)) {
		_mm_errno = MMERR_UNKNOWN_WAVE_TYPE;
		return NULL;
	}

	wh.wFormatTag      = _mm_read_I_UWORD(fp);
	wh.nChannels       = _mm_read_I_UWORD(fp);
	wh.nSamplesPerSec  = _mm_read_I_ULONG(fp);
	wh.nAvgBytesPerSec = _mm_read_I_ULONG(fp);
	wh.nBlockAlign     = _mm_read_I_UWORD(fp);
	wh.nFormatSpecific = _mm_read_I_UWORD(fp);

	if(feof(fp)) {
		_mm_errno = MMERR_UNKNOWN_WAVE_TYPE;
		return NULL;
	}

	/* skip other crap */
	_mm_fseek(fp,wh.fLen-16,SEEK_CUR);
	_mm_read_string(dID,4,fp);

	if(memcmp(dID,"data",4)) {
		_mm_errno = MMERR_UNKNOWN_WAVE_TYPE;
		return NULL;
	}

	if(wh.nChannels>1) {
		_mm_errno = MMERR_UNKNOWN_WAVE_TYPE;
		return NULL;
	}

#ifdef MIKMOD_DEBUG
	fprintf(stderr,"\rwavloader : wFormatTag=%04x blockalign=%04x nFormatSpc=%04x\n",
	        wh.wFormatTag,wh.nBlockAlign,wh.nFormatSpecific);
#endif

	if(!(si=(SAMPLE*)_mm_malloc(sizeof(SAMPLE)))) return NULL;

	si->speed  = wh.nSamplesPerSec/wh.nChannels;
	si->volume = 64;
	si->length = _mm_read_I_ULONG(fp);

	if(wh.nBlockAlign == 2) {
		si->flags    = SF_16BITS | SF_SIGNED;
		si->length >>= 1;
	}

	SL_RegisterSample(si,MD_SNDFX,fp);
	SL_LoadSamples();

	return si;
}

SAMPLE* Sample_Load(CHAR* filename)
{
	FILE *fp;
	SAMPLE *si;

	if(!(md_mode & DMODE_SOFT_SNDFX)) return NULL;
	if(!(fp=_mm_fopen(filename,"rb"))) return NULL;

	si = Sample_LoadFP(fp);
	fclose(fp);

	return si;
}

void Sample_Free(SAMPLE* si)
{
	if(si) {
		MD_SampleUnload(si->handle);
		free(si);
	}
}

