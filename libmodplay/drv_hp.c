/*	MikMod sound library
	(c) 1998 Miodrag Vallat and others - see file AUTHORS for complete list

	This library is free software; you can redistribute it and/or modify
	it under the terms of the GNU Library General Public License as
	published by the Free Software Foundation; either version 2 of
	the License, or (at your option) any later version.
 
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Library General Public License for more details.
 
	You should have received a copy of the GNU Library General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/
  
/*==============================================================================

  $Id$

  Mikmod driver for output to HP 9000 series /dev/audio

==============================================================================*/

/*

	Written by Lutz Vieweg <lkv@mania.robin.de>

*/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif
#include <stdlib.h>
#include <sys/audio.h>
#include <sys/ioctl.h>

#include <mikmod_internals.h>

#define BUFFERSIZE 32768

static	int fd=-1;
static	SBYTE *audiobuffer=NULL;

static BOOL HP_IsThere(void)
{
	return 1;
}

static BOOL HP_Init(void)
{
	int flags;
	
	if (!(md_mode&DMODE_16BITS)) {
		_mm_errno=MMERR_16BIT_ONLY;
		return 1;
	}

	if ((fd=open("/dev/audio",O_WRONLY|O_NDELAY,0))<0) {
		_mm_errno=MMERR_OPENING_AUDIO;
		return 1;
	}

	if ((flags=fcntl(fd,F_GETFL,0))<0) {
		_mm_errno=MMERR_NON_BLOCK;
		return 1;
	}
	flags|=O_NDELAY;
	if (fcntl(fd,F_SETFL,flags)<0) {
		_mm_errno=MMERR_NON_BLOCK;
		return 1;
	}
	
	if (ioctl(fd,AUDIO_SET_DATA_FORMAT,AUDIO_FORMAT_LINEAR16BIT)) {
		_mm_errno=MMERR_HP_SETSAMPLESIZE;
		return 1;
	}
	
	if (ioctl(fd,AUDIO_SET_SAMPLE_RATE,md_mixfreq)) {
		_mm_errno=MMERR_HP_SETSPEED;
		return 1;
	}
	
	if (ioctl(fd,AUDIO_SET_CHANNELS,(md_mode&DMODE_STEREO)?2:1)) {
		_mm_errno=MMERR_HP_CHANNELS;
		return 1;
	}
	
	/* choose between:
		AUDIO_OUT_SPEAKER
		AUDIO_OUT_HEADPHONE
		AUDIO_OUT_LINE       */
	if (ioctl(fd,AUDIO_SET_OUTPUT,
	             AUDIO_OUT_SPEAKER|AUDIO_OUT_HEADPHONE|AUDIO_OUT_LINE)) {
		_mm_errno=MMERR_HP_AUDIO_OUTPUT;
		return 1;
	}

{
	struct audio_describe description;
	struct audio_gains gains;
	float volume=1.0;

	if (ioctl(fd,AUDIO_DESCRIBE,&description)) {
		_mm_errno=MMERR_HP_AUDIO_DESC;
		return 1;
	}
	if (ioctl(fd,AUDIO_GET_GAINS,&gains)) {
		_mm_errno=MMERR_HP_GETGAINS;
		return 1;
	}

	gains.transmit_gain=(int)((float)description.min_transmit_gain+
	  (float)(description.max_transmit_gain-description.min_transmit_gain)
	  * volume);
	if (ioctl(fd,AUDIO_SET_GAINS,&gains)) {
		_mm_errno=MMERR_HP_SETGAINS;
		return 1;
	}
}
	
	if (ioctl(fd,AUDIO_SET_TXBUFSIZE,BUFFERSIZE*8)) {
		_mm_errno=MMERR_HP_BUFFERSIZE;
		return 1;
	}

	if (!(audiobuffer=(SBYTE*)_mm_malloc(BUFFERSIZE))) return 1;
	
	return VC_Init();
}

static void HP_Exit(void)
{
	VC_Exit();
	if (fd>=0) {
		close(fd);
		fd=-1;
	}
	if (audiobuffer) {
		free(audiobuffer);
		audiobuffer=NULL;
	}
}

static BOOL HP_Reset(void)
{
	HP_Exit();
	return HP_Init();
}

static void HP_Update(void)
{
	write(fd,audiobuffer,VC_WriteBytes(audiobuffer,BUFFERSIZE));
}

MDRIVER drv_hp={
	NULL,
	"HP-UX Audio",
	"HP-UX Audio driver v1.2",
	0,255,
	HP_IsThere,
	VC_SampleLoad,
	VC_SampleUnload,
	VC_SampleSpace,
	VC_SampleLength,
	HP_Init,
	HP_Exit,
	HP_Reset,
	VC_SetNumVoices,
	VC_PlayStart,
	VC_PlayStop,
	HP_Update,
	VC_VoiceSetVolume,
	VC_VoiceSetFrequency,
	VC_VoiceSetPanning,
	VC_VoicePlay,
	VC_VoiceStop,
	VC_VoiceStopped,
	VC_VoiceGetPosition,
	VC_VoiceRealVolume
};
