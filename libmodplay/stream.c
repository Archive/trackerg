/*==============================================================================

  $Id$

  Streaming Audio module for MikMod.

==============================================================================*/

/*
	This library is free software; you can redistribute it and/or modify
	it under the terms of the GNU Library General Public License as
	published by the Free Software Foundation; either version 2 of
	the License, or (at your option) any later version.
 
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Library General Public License for more details.
 
	You should have received a copy of the GNU Library General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <mikmod.h>

static	BOOL is_streaming = 0;
static	MSTREAM *firststream = NULL;
static	BOOL active;

void MikMod_RegisterStream(MSTREAM* stream)
{
	MSTREAM *cruise=firststream;

    /* if we try to register an invalid stream loader, or an already registered
	   stream loader, ignore this attempt */
    if ((!stream)||(stream->next))
        return;

	if(cruise) {
		while(cruise->next) cruise = cruise->next;
		cruise->next = stream;
	} else
		firststream = stream;
}

BOOL Stream_PlayFN(CHAR* filename)
{
	return 0;
}

BOOL Stream_PlayFP(FILE* fp)
{
	BOOL ok=0;
	MSTREAM *l;

	_mm_errno = _mm_critical = 0;
	_mm_iobase_setcur(fp);

	/* Try to find a loader that recognizes the stream */
	for(l=firststream;l;l=l->next) {
		_mm_rewind(fp);
		if(l->Test()) break;
	}

	if(!l) {
		_mm_errno = MMERR_NOT_A_STREAM;
		if(_mm_errorhandler) _mm_errorhandler();
		_mm_iobase_revert();
		return 1;
	}

	/* init stream loader and load the header */
	if(l->Init()) {
		_mm_rewind(fp);
		ok = l->Load();
	}

	/* free loader allocations */
	l->Cleanup();

	if(!ok) {
		_mm_iobase_revert();
		return 1;
	}

	_mm_iobase_revert();
	active = 1;
	return 0;
}

BOOL Stream_Init(ULONG speed, UWORD flags)
{
	if(is_streaming) md_driver->StreamExit();
	if(md_driver->StreamInit(speed,flags)) return 1;
	is_streaming = 1;

	return 0;
}

void Stream_Exit(void)
{
	if(is_streaming) md_driver->StreamExit();
	is_streaming = 0;
}

void Stream_Update(void)
{
static	ULONG last = 0;
	ULONG curr,todo;

	curr=md_driver->StreamGetPosition();
	if(curr==last) return;

	if(curr>last) {
		todo = curr-last;
		last += todo;
	} else
		last = curr;
}
