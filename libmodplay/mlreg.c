/*	MikMod sound library
	(c) 1998 Miodrag Vallat and others - see file AUTHORS for complete list

	This library is free software; you can redistribute it and/or modify
	it under the terms of the GNU Library General Public License as
	published by the Free Software Foundation; either version 2 of
	the License, or (at your option) any later version.
 
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Library General Public License for more details.
 
	You should have received a copy of the GNU Library General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

/*==============================================================================

  $Id$

  Routine for registering all loaders in MikMod for the current platform.

==============================================================================*/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <mikmod_internals.h>

void MikMod_RegisterAllLoaders(void)
{
	MikMod_RegisterLoader(&load_669);
	MikMod_RegisterLoader(&load_amf);
	MikMod_RegisterLoader(&load_dsm);
	MikMod_RegisterLoader(&load_far);
	MikMod_RegisterLoader(&load_it);
	MikMod_RegisterLoader(&load_mod);
	MikMod_RegisterLoader(&load_med);
	MikMod_RegisterLoader(&load_mtm);
	MikMod_RegisterLoader(&load_s3m);
	MikMod_RegisterLoader(&load_stm);
	MikMod_RegisterLoader(&load_ult);
	MikMod_RegisterLoader(&load_xm);

	MikMod_RegisterLoader(&load_m15);
}
