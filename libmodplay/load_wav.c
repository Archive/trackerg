/*==============================================================================

  $Id$

  WAV Streaming Audio Loader / Player

==============================================================================*/

/*
	This library is free software; you can redistribute it and/or modify
	it under the terms of the GNU Library General Public License as
	published by the Free Software Foundation; either version 2 of
	the License, or (at your option) any later version.
 
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Library General Public License for more details.
 
	You should have received a copy of the GNU Library General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <string.h>

#include <mikmod.h>

/*========== File information */

typedef struct WAV {
	CHAR  rID[4];
	ULONG rLen;
	CHAR  wID[4];
	CHAR  fID[4];
	ULONG fLen;
	UWORD wFormatTag;
	UWORD nChannels;
	ULONG nSamplesPerSec;
	ULONG nAvgBytesPerSec;
	UWORD nBlockAlign;
	UWORD nFormatSpecific;
} WAV;

/*========== Loader code */

BOOL WAV_Load(void)
{
#if 0
	WAV wh;
	CHAR dID[4];
	SAMPLE *si;

	/* read wav header */
	_mm_read_string(wh.rID,4,stream_fp);
	wh.rLen            = _mm_read_I_ULONG(stream_fp);
	_mm_read_string(wh.wID,4,stream_fp);
	_mm_read_string(wh.fID,4,stream_fp);
	wh.fLen            = _mm_read_I_ULONG(stream_fp);
	wh.wFormatTag      = _mm_read_I_UWORD(stream_fp);
	wh.nChannels       = _mm_read_I_UWORD(stream_fp);
	wh.nSamplesPerSec  = _mm_read_I_ULONG(stream_fp);
	wh.nAvgBytesPerSec = _mm_read_I_ULONG(stream_fp);
	wh.nBlockAlign     = _mm_read_I_UWORD(stream_fp);
	wh.nFormatSpecific = _mm_read_I_UWORD(stream_fp);

	/* check it */
	if((feof(stream_fp))||(memcmp(wh.rID,"RIFF",4))||(memcmp(wh.wID,"WAVE",4))||
	   (memcmp(wh.fID,"fmt ",4))) {
		_mm_errno = MMERR_UNKNOWN_WAVE_TYPE;
		return 1;
	}

	/* skip unused stuff */
	_mm_fseek(stream_fp,wh.fLen-16,SEEK_CUR);
	_mm_read_string(dID,4,stream_fp);

	if(memcmp(dID,"data",4)) {
		_mm_errno = MMERR_UNKNOWN_WAVE_TYPE;
		return 1;
	}

	if(wh.nChannels>1) {
		_mm_errno = MMERR_UNKNOWN_WAVE_TYPE;
		return 1;
	}

#ifdef MIKMOD_DEBUG
	fprintf(stderr,"WAV:wFormatTag=%04x, blockalign=%04x, nFormatSpc=%04x\n",
	        wh.wFormatTag,wh.nBlockAlign,wh.nFormatSpecific);
#endif

	if(!(si=(SAMPLE*)_mm_malloc(sizeof(SAMPLE)))) return 1;
	si->speed  = wh.nSamplesPerSec/wh.nChannels;
	si->volume = 64;
	si->length = _mm_read_I_ULONG(stream_fp);
	if(wh.nBlockAlign==2) {
		si->flags = SF_16BITS | SF_SIGNED;
		si->length>>=1;
	}
	return 0;
#endif
}
