/*	MikMod sound library
	(c) 1998 Miodrag Vallat and others - see file AUTHORS for complete list

	This library is free software; you can redistribute it and/or modify
	it under the terms of the GNU Library General Public License as
	published by the Free Software Foundation; either version 2 of
	the License, or (at your option) any later version.
 
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Library General Public License for more details.
 
	You should have received a copy of the GNU Library General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

/*==============================================================================

  $Id$

  Mikmod driver for output to a file called MUSIC.WAV

==============================================================================*/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#include <mikmod_internals.h>

#define BUFFERSIZE 32768
#define FILENAME "music.wav"

static	FILE *wavout=NULL;
static	SBYTE *audiobuffer=NULL;
static	ULONG dumpsize;

static void putheader(void)
{
	_mm_write_string("RIFF    WAVEfmt ",wavout);
	_mm_write_I_ULONG(16,wavout);	/* length of this RIFF block crap */

	_mm_write_I_UWORD(1, wavout);	/* microsoft format type */
	_mm_write_I_UWORD((md_mode&DMODE_STEREO)?2:1,wavout);
	_mm_write_I_ULONG(md_mixfreq,wavout);
	_mm_write_I_ULONG(md_mixfreq*((md_mode&DMODE_STEREO)?2:1)*
	                  ((md_mode&DMODE_16BITS)?2:1),wavout);
	/* block alignment (8/16 bit) */
	_mm_write_I_UWORD(((md_mode&DMODE_16BITS)?2:1)* 
	                  ((md_mode&DMODE_STEREO)?2:1),wavout);
	_mm_write_I_UWORD((md_mode&DMODE_16BITS)?16:8,wavout);
	_mm_write_string("data",wavout);
}

static BOOL WAV_IsThere(void)
{
	return 1;
}

static BOOL WAV_Init(void)
{
	if(!(wavout=fopen(FILENAME, "wb"))) {
		_mm_errno=MMERR_OPENING_FILE;
		return 1;
	}
	if(!(audiobuffer=(SBYTE*)_mm_malloc(BUFFERSIZE))) {
		fclose(wavout);unlink(FILENAME);
		wavout=NULL;
		return 1;
	}

	md_mode|=DMODE_SOFT_MUSIC|DMODE_SOFT_SNDFX;

	if (VC_Init()) {
		fclose(wavout);unlink(FILENAME);
		wavout=NULL;
		return 1;
	}
	putheader();
	dumpsize=0;

	return 0;
}

static void WAV_Exit(void)
{
	VC_Exit();

	/* write in the actual sizes now */
	if(wavout) {
		_mm_fseek(wavout,4,SEEK_SET);
		_mm_write_I_ULONG(dumpsize+32,wavout);
		_mm_fseek(wavout,40,SEEK_SET);
		_mm_write_I_ULONG(dumpsize,wavout);
		fclose(wavout);
		wavout=NULL;
	}
	if(audiobuffer) {
		free(audiobuffer);
		audiobuffer=NULL;
	}
}

static void WAV_Update(void)
{
	ULONG done;

	done=VC_WriteBytes(audiobuffer,BUFFERSIZE);
	fwrite(audiobuffer,1,done,wavout);
	dumpsize+=done;
}

static BOOL WAV_Reset(void)
{
	fclose(wavout);
	if(!(wavout=fopen(FILENAME, "wb"))) {
		_mm_errno=MMERR_OPENING_FILE;
		return 1;
	}
	putheader();
	dumpsize=0;

	return 0;
}

MDRIVER drv_wav={
	NULL,
	"Disk writer (wav)",
	"Wav disk writer (music.wav) v1.1",
	0,255,
	WAV_IsThere,
	VC_SampleLoad,
	VC_SampleUnload,
	VC_SampleSpace,
	VC_SampleLength,
	WAV_Init,
	WAV_Exit,
	WAV_Reset,
	VC_SetNumVoices,
	VC_PlayStart,
	VC_PlayStop,
	WAV_Update,
	VC_VoiceSetVolume,
	VC_VoiceSetFrequency,
	VC_VoiceSetPanning,
	VC_VoicePlay,
	VC_VoiceStop,
	VC_VoiceStopped,
	VC_VoiceGetPosition,
	NULL
};
